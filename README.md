# AirportsInformation

This project aims to test Airports API with cucumber and restAssured.

### Airports API
This API returns the airports details of all airports available. The api provides the options to search for airports. 
The API definition is available in [here](https://gitlab.com/priya.murugavel89/airportsinformation/-/blob/master/Airports%20API%20v2.openapi.yaml)

**API's tested in this project:**
* RetrieveAllAirports - GET https://api.transavia.com/v2/airports/
* RetrieveAirportsByCountryCode - GET https://api.transavia.com/v2/airports/countrycode/{countryCode} where country code - NL, DE, FR, ES
* CreateContacts - POST https://www.callfire.com/v2/contacts

###Prerequisites: Tools/Framework/Libraries

* Eclipse/intellij IDE
* JDK 8
* Maven
* JUnit
* Cucumber - BDD/Gherkin style feature files
* Rest assured - Rest api verification library
* maven-surefire - To generate html report
* maven-site-plugin - To generate html report in site folder

###Project Structure

**Each test case contains:**
* Feature file - To describe the scenario to retrieve AirportsInformation and validating the responses
* StepDefinition Class - Each step in the feature file is tied to the step definition file
* Runner Class - To execute the tests or scenarios mentioned in the feature file


`RetrieveAllAirports.feature:`

This feature file describes the positive and negative scenarios for RetrieveAllAirports API:

**Positive scenarios** - To retrieve all airport information when hit the api key 

######Validations: 

*Request* - valid api key, header is Content-Type 

*Response* - header is Content-Type, statusCode is 200, responseBody has expected number of airports and match details of first array list

**Negative scenario** - To retrieve airport information without api key

######Validations:

*Request* - header is Content-Type 

*Response* - header is Content-Type, statusCode is 401, responseBody has error message and error status code
 

`RetrieveAirportsByCountryCode.feature:`

This feature file describes the positive and negative scenarios for RetrieveAirportsByCountryCode API:

**Positive scenario** -  To retrieve airport information using country code

######Validations: 

*Request* - valid api key, header is Content-Type, valid country code

*Response* - header is Content-Type, statusCode is 200, responseBody has expected number of airports in selected country and matches city name

**Negative scenario** - To retrieve airport information using invalid country code

######Validations: 

*Request* - header is Content-Type 

*Response* - header is Content-Type, statusCode is 400, responseBody has error message

`createcontacts.feature:`

This feature file describes the positive and negative scenarios for CreateContacts API:

**NOTE: Since Airport API does not have any post, to demonstrate the POST and Basic authentication used CallFire API**

**Positive scenario** -  To create contact using phone number

######Validations: 

*Request* - valid username and password, header is content-type

*Response* - statusCode is 200

**Negative scenario** - To create contact using phone number using invalid phone number

######Validations: 

*Request* - valid username and password, header is content-type 

*Response* - statusCode is 400, responseBody has error message

###Steps to execute the project:

**To run locally**: 
* Clone from repository from https://gitlab.com/priya.murugavel89/airportsinformation
* Go to project root and execute the command - **mvn test site**

**To view Test Reports:**
* Html reports can be found under `target/site/index.html`
* cucumber reports can be found under `target/cucumber/index.html`
* xml reports can be found under `target/surefire-reports/TEST-*.xml`

** To run in Gitlab: **
Navigate to project repository https://gitlab.com/priya.murugavel89/airportsinformation
Pipelines can be found under CI/CD - Pipelines

**Reports:**
XML reports are integrated with pipelines using .gitlab-ci.yml file. So these reports are found in gitlab while executing tests through pipelines


