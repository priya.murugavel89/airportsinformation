package nl.anyapi.transavia.airports.getairportbycode;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.anyapi.transavia.airports.utils.TestBase;

public class RetrieveAirportsByCountryCodeStepDef extends TestBase {

	@Given("^I have API$")
	public void i_have_API() {
		req_spec = given().header("apikey", "75171ae1bb30446b8d17f0cae14df795")
				.header("Content-Type", "application/json").baseUri(baseUrl);
	}

	@When("^I Hit API with CountryCode as \"([^\"]*)\"$")
	public void i_Hit_API_with_CountryCode_as(String cntryCode) {
		response = req_spec.when().get("/countrycode/" + cntryCode);

	}

	@Then("^API should return success statusCode with numberOfAirports \"([^\"]*)\" and cityName \"([^\"]*)\"$")
	public void api_should_return_success_statusCode_with_numberOfAirports_and_cityName(Integer noOfAirports,
			String cityName) {

		validatable_resp = response.then()
				.statusCode(200).and()
				.body("size()", equalTo(noOfAirports)).and()
				.body("city[0]", equalTo(cityName));
	}

	@When("^I Hit API with invalid CountryCode as \"([^\"]*)\"$")
	public void i_Hit_API_with_invalid_CountryCode_as(String cntryCode) {
		response = req_spec.when().get("/countrycode/" + cntryCode);

	}

	@Then("^API should return error statusCode and error message \"([^\"]*)\"$")
	public void api_should_return_error_statusCode_and_error_message(String errMsg) {

		validatable_resp = response.then()
				.header("Content-Type", "application/json; charset=utf-8").and()
				.statusCode(400).and()
				.body("errorMessages[0]", equalTo(errMsg));

	}

}
