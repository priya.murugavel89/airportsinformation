package nl.anyapi.transavia.airports.getairportbycode;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(features= {"classpath:features"},
glue= {"nl.anyapi.transavia.airports.getairportbycode"},
monochrome=true,
tags= "@countrycode",
plugin= {"pretty","html:target/cucumber"})
public class RunCuke {

}
