package nl.anyapi.transavia.airports.getallairports;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import nl.anyapi.transavia.airports.utils.TestBase;

public class RetrieveAllAirportsStepDef extends TestBase{	
	
	@Given("^I hit the api with GET endpoint$")
	public void i_hit_the_api_with_GET_endpoint() {
		response = given()
				.header("apikey", "75171ae1bb30446b8d17f0cae14df795")
				.header("Content-Type","application/json")
				.get(baseUrl);
	}	

	@Then("^api should return all airports$")
	public void api_should_return_all_airports(){
	
		int expectedNoOfAirports = 109;
		
		validatable_resp = response.then()
				  .header("Content-Type","application/json; charset=utf-8").and()
				  .statusCode(200).and()
				  .body("size()", equalTo(expectedNoOfAirports)).and()
				  .body("[0].id",equalTo("ACE")).and()
				  .body("[0].name",equalTo("Arrecife (Lanzarote)")).and()
				  .body("[0].city",equalTo("Arrecife (Lanzarote)")).and()
				  .body("[0].country.code",equalTo("ES")).and()
				  .body("[0].country.name",equalTo("Spain"));

	}	
	
	@Given("^I hit the api with GET endpoint without apikey$")
	public void i_hit_the_api_with_GET_endpoint_without_apikey() {
		response = given()
				.header("Content-Type","application/json")
				.get(baseUrl);
	}	
	
	@Then("^api should return unauthorized$")
	public void api_should_return_unauthorized(){
			
		validatable_resp = response.then()
				  .header("Content-Type","application/json").and()
				  .statusCode(401).and()
				  .body("statusCode",equalTo(401)).and()
				  .body("message",equalTo("Access denied due to missing subscription key. Make sure to include subscription key when making requests to an API."));

	}	
}
