package nl.anyapi.transavia.airports.utils;

import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class TestBase {

	protected static String baseUrl="https://api.transavia.com/v2/airports";
	protected static String baseUrlCallFire="https://www.callfire.com/v2/contacts";
	protected static RequestSpecification req_spec=null;
	protected static Response response=null;
	protected static ValidatableResponse validatable_resp = null;
		
	}

