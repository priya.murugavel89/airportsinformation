package nl.anyapi.zcallfire.createcontacts;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "classpath:features" },
				glue = {"nl.anyapi.zcallfire.createcontacts" },
				monochrome = true,
				tags = "@create",
				plugin = { "pretty", "html:target/cucumber" })
public class RunCuke {

}
