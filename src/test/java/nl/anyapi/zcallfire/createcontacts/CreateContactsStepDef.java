package nl.anyapi.zcallfire.createcontacts;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.anyapi.transavia.airports.utils.TestBase; 

public class CreateContactsStepDef extends TestBase{
	
	@Given("I have API")
	public void i_have_API() {
		req_spec = given()
				.header("content-type", "application/json")
				.auth()
				.basic("f23562a750a6", "1ac322ef11f317ab");
	}

	@When("^I Hit API with PhoneNumber as \"([^\"]*)\"$")
	public void i_Hit_API_with_PhoneNumber_as(String phoneNo) {
	
		JSONObject json = new JSONObject();
		json.put("homePhone", phoneNo);
		
		JSONArray array = new JSONArray();
		array.add(json);
		
		req_spec.body(array.toJSONString());
		
		response = req_spec.when().post(baseUrlCallFire);
	}
	
	@Then("API should return success statusCode")
	public void api_should_return_success_statusCode() {
		
		validatable_resp = response.then()
				.statusCode(200);
	}

	@When("^I Hit API with invalid PhoneNumber as \"([^\"]*)\"$")
	public void i_Hit_API_with_invalid_PhoneNumber_as(Integer phoneNo) {
		
		JSONObject json = new JSONObject();
		json.put("extraPhone2", phoneNo);
		
		JSONArray array = new JSONArray();
		array.add(json);
		
		req_spec.body(array.toJSONString());
		
		response = req_spec.when().post(baseUrlCallFire);
	}
	
	@Then("API should return error statusCode and errorMessage {string}")
	public void api_should_return_error_statusCode_and_errorMessage(String errMsg) {
		
		validatable_resp = response.then()
			.statusCode(400)
			.body("message", equalTo(errMsg));
		    
	}
}
