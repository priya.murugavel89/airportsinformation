@all
Feature: Testing Airport API to Retrieve All Airports

Scenario: GET request to retrieve all airports in Airport API
	Given I hit the api with GET endpoint
	Then api should return all airports
	
Scenario: GET request to retrieve all airports in Airport API without APIKEY
	Given I hit the api with GET endpoint without apikey
	Then api should return unauthorized
	