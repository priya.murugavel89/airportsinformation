@countrycode
Feature: Testing Positive and Negative Testcases for Airport API to Retrieve Airports based on CountryCode
	
Background:
	Given I have API
	
Scenario Outline: GET request to retrieve airports for valid CountryCode - <countryCode>
	When I Hit API with CountryCode as "<countryCode>"
	Then API should return success statusCode with numberOfAirports "<numOfAirports>" and cityName "<cityName>"
	
Examples:
	|countryCode|numOfAirports|cityName |
	|NL		    |3			  |Amsterdam|
	|DE			|1			  |Berlin	|
	|FR			|17			  |Ajaccio	|


Scenario Outline: GET request to retrieve airports based on invalid CountryCode - <countryCode>
	When I Hit API with invalid CountryCode as "<countryCode>"
	Then API should return error statusCode and error message "<errorMessage>"
	
Examples:
	|countryCode|errorMessage                  		   	 								|
	|IND		|CountryCode must be a valid country code (2 letters). Valid example: NL|
	|!@			|CountryCode must be a valid country code (2 letters). Valid example: NL|
	|DE,NL,FR,ES|Error: More than 3 countries is not allowed							|
	