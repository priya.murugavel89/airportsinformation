@create
Feature: Testing Positive and Negative Testcases for CallFire API to create contacts based on PhoneNumber
	
Background:
	Given I have API
	
Scenario Outline: POST request to create contacts for valid PhoneNumber - <phoneNumber>
	When I Hit API with PhoneNumber as "<phoneNumber>"
	Then API should return success statusCode
	
Examples:
	|phoneNumber|
	|12345678911|
	|12345678922|


Scenario Outline: POST request to create contacts for invalid PhoneNumber - <phoneNumber>
	When I Hit API with invalid PhoneNumber as "<phoneNumber>"
	Then API should return error statusCode and errorMessage "<errorMessage>"
	
Examples:
	|phoneNumber|errorMessage                  		   	 							|
	|1111		|Contact contain invalid phone number(s): {  \"extraPhone2\" : \"1111\"}|